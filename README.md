# Simple Jobs API

# Run project
In console (PyCharm: Alt+F12) run: `uvicorn main:app --reload`

More details: https://fastapi.tiangolo.com/tutorial/first-steps/
# Other

Locust help:

* [Writing a locustfile](https://github.com/locustio/locust/blob/master/docs/writing-a-locustfile.rst).
* Run locust: locust --host=http://localhost:8000
* Test locust: http://localhost:8089/
