import re


def check_date(vacancies, datePattern):
    if not vacancies:
        return False

    if not datePattern:
        return False

    flag = True
    for vacancy in vacancies:
        if re.search(datePattern, str(vacancy['pub_date'])) is None:
            flag = False
            break
        if not flag:
            break

    return flag


def check_title(vacancies, titlePattern):
    if not vacancies:
        return False

    if not titlePattern:
        return False

    flag = True
    for vacancy in vacancies:
        if re.search(titlePattern, str(vacancy['title'])) is None:
            flag = False
            break
        if not flag:
            break

    return flag
