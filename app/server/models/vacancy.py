from typing import Optional

from pydantic import BaseModel, Field


# according to tutorial: https://testdriven.io/blog/fastapi-mongo/
class VacancySchema(BaseModel):
    about_proj: str = Field(...)  # string, req
    categories: str = Field(...)  # string, req
    company: str = Field(...)  # string, req
    company_url: str = Field(...)  # string, req
    id: int = Field(..., gt=0, lt=9)  # int, req
    location: str = Field(...)    # string, req
    offer: str = Field(None)    # string, non req
    plus_skills: str = Field(None)    # string, non req
    pub_date: str = Field(...)    # string, non
    required_skills: str = Field(None)    # string, non req
    salary: str = Field(None)    # string, non req
    time_of_update: str = Field(...)    # datetime, req
    title: str = Field(...)    # string, req
    url: str = Field(...)    # string, req

    class Config:
        schema_extra = {
            "example": {
                "about_proj": "On behalf of ,  X-company is looking for a , to ...",
                "categories": " content+solutions, cnet, python",
                "company": "X-company",
                "company_url": "https://jobs.dou.ua/companies/***/",
                "id": "00000",
                "location": " Харьков",
                "offer": None,
                "plus_skills": None,
                "pub_date": "2019-03-19",
                "required_skills": " ",
                "salary": None,
                "time_of_update": "2019-03-21 19:08:07",
                "title": "Junior Software Engineer (Python) for CNET Co...",
                "url": "https://jobs.dou.ua/companies/X-company/vacancies",
            }
        }


def ResponseModel(data, message):
    return {
        "data": [data],
        "code": 200,
        "message": message,
    }


def ErrorResponseModel(error, code, message):
    return {"error": error, "code": code, "message": message}
