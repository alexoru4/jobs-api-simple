import uvicorn


if __name__ == "__main__":
    uvicorn.run("server.app:app", host="localhost", port=8000, reload=True, limit_max_requests=99999,
                limit_concurrency=99999, timeout_keep_alive=500, log_level="warning")
